<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class GooglePoint extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'google_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','coordinates','altitude','category_id','user_id'];

    /**
     * Define the name of column in a table.
     *
     * @var string
     */
    protected $column = 'coordinates';

    /**
     * Select geometrical attributes as text from database.
     *
     * @var bool
     */
    protected $geometryAsText = true;

    /**
     * Get a new query builder for the model's table.
     * Manipulate in case we need to convert geometrical fields to text.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function newQuery()
    {

        if ($this->geometryAsText === true) {
            return parent::newQuery()->select('*',
                DB::raw('X(' . $this->column . ') AS lat, 
                    Y(' . $this->column . ') AS lng'));
        }

    }

    /**
     * Get category name of point
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() {
        return $this->belongsTo('App\CatPoints', 'category_id', 'id');
    }


    /**
     * Scope a query to only include public and visible resources.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query->where('public', 1)->where('visible', 1);
    }

    /**
     * Scope a query to only include private resources.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePrivate($query)
    {
        return $query->where('user_id', Auth::id());
    }
}
