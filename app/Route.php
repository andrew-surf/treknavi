<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Route extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','name','description','category','distance','min_alt','max_alt','elevation_gain','duration'];

    /**
     * Get the points for the route.
     */
    public function points()
    {
        return $this->hasMany('App\RoutePoints', 'route_id', 'id');
    }

    /**
     * Get distance.
     *
     * @param  integer $value
     * @return integer
     */
    public function getDistanceAttribute($value)
    {
        $distance = number_format($value / 1000, 1);
        return $distance;
    }

    /**
     * Get description.
     *
     * @param  string $value
     * @return string
     */
    public function getDescriptionAttribute($value)
    {
        $description = str_replace('\'','"', $value);
        $description = str_replace(["\n", "\r"], '', $description);
        return $description;
    }

    /**
     * Get time.
     *
     * @param  integer $value
     * @return string
     */
    public function getDurationAttribute($value)
    {
        if($value !== null) {
            $hours = floor($value / 3600);
            $minutes = floor(($value / 60) % 60);
            $seconds = $value % 60;

            return "$hours:$minutes:$seconds";
        }

        return $value;
    }

    /**
     * Scope a query to only include public and visible resources.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query->where('public', 1);
    }

    /**
     * Scope a query to only include private resources.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePrivate($query)
    {
        return $query->where('user_id', Auth::id());
    }
}
