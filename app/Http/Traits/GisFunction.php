<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;
use App\Route;

trait GisFunction
{
    /**
     * Get polylines by points
     *
     * @param $route
     * @return array
     */
    public function getPolylines($route)
    {
        $points = $route->points;

        foreach ($points as $point) {
            $polyline[] = ['latitude' => $point->lat, 'longitude' => $point->lng];
        }

        return $polyline;
    }

    /**
     * Get nearest google points
     *
     * @param $points
     * @param $nth
     * @param $meters
     * @return array
     */
    public function getNearestPoints($points, $nth, $meters)
    {
        $arr = [];
        foreach ($points as $k => $point) {
            // Get every nth point
            if ($k % $nth == 0) {
                $gPoints = DB::table('google_points')
                    ->select('id', 'category_id', 'name', DB::raw('st_x(coordinates) AS lat, st_y(coordinates) AS lng'))
                    ->whereRaw('ST_Distance_Sphere(coordinates, POINT(' . $point->lat . ', ' . $point->lng . ')) < ' . $meters . '')
                    ->get();

                foreach ($gPoints as $gPoint) {
                    $arr[] = [
                        'id' => $gPoint->id,
                        'category_id' => $gPoint->category_id,
                        'name' => $gPoint->name,
                        'lat' => $gPoint->lat,
                        'lng' => $gPoint->lng
                    ];
                }
            }
        }

        // Remove dublicate points
        $output = array_map("unserialize", array_unique(array_map("serialize", $arr)));

        return $output;
    }

    /**
     * Get nearest routes
     *
     * @param $point
     * @param $meters
     * @return array
     */
    public function getNearestRoutes($point, $meters)
    {
        $gPoints = DB::table('route_points')
            ->select('route_id', DB::raw('st_x(coordinates) AS lat, st_y(coordinates) AS lng'))
            ->whereRaw('ST_Distance_Sphere(coordinates, POINT(' . $point->lat . ', ' . $point->lng . ')) < ' . $meters . '')
            ->groupBy('route_id')
            ->get();

        foreach ($gPoints as $gPoint) {
            $arr[] = $gPoint->route_id;
        }

        $routes = Route::with('points')->where('public', '1')->find($arr);

        return $routes;
    }
}