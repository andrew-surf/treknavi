<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GooglePoint;
use App\Route;

class AjaxController extends Controller
{
    /**
     * Get point's info
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPointInfo(Request $request)
    {
        $point = GooglePoint::find($request->get('id'))->toArray();
        unset($point['coordinates']);

        return response()->json($point);
    }
}
