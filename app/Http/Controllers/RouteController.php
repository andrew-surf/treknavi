<?php

namespace App\Http\Controllers;

use App\Route;
use App\RoutePoints;
use Illuminate\Http\Request;
use phpGPX\Models\Point;
use phpGPX\phpGPX;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mapper;
use App\Http\Traits\GisFunction;

class RouteController extends Controller
{
    use GisFunction;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $prefix = $request->route()->getPrefix();
        if ($prefix == '/user') {
            $routes = Route::where('user_id', Auth::id())->paginate(30);
            return view('pages.route.user-routes-index', compact('routes'));
        } else {
            $routes = Route::where('public', '1')->paginate(30);
            return view('pages.route.public-routes-index', compact('routes'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Mapper::map(53.250981071172895, 50.265240499999976, ['zoom' => 10, 'center' => true, 'setmarker' => true]);

        return view('pages.route.user-route-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'file' => 'required|file|max:1024'
        ]);

        $gpx = new phpGPX();
        $file = $gpx->load($request->file);

        foreach ($file->tracks as $track) {
            $stat = $track->stats->toArray();

            $data_route = [
                'user_id' => Auth::id(),
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'distance' => intval($stat['distance']),
                'min_alt' => $stat['minAltitude'],
                'max_alt' => $stat['maxAltitude'],
                'elevation_gain' => $stat['cumulativeElevationGain'],
                'duration' => $stat['duration']
            ];

            $route = new Route($data_route);
            $route->save();

            $points = $track->getPoints();

            foreach ($points as $point) {
                $data_points[] = [
                    'route_id' => $route->id,
                    'coordinates' => DB::raw("GeomFromText('POINT(" . $point->latitude . " " . $point->longitude . ")')"),
                    'altitude' => $point->elevation,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            RoutePoints::insert($data_points);
        }

        return redirect('/user/routes/'. $route->id)->with('success', 'Трек сохранен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $prefix = $request->route()->getPrefix();
        if ($prefix == '/user') {
            $route = Route::with('points')->where('user_id', Auth::id())->findOrFail($id);
            $points = $route->points;

            // get first point and show the map
            Mapper::map($points[0]->lat, $points[0]->lng, ['center' => true]);

            // Get polyline and nearest points (<=2000m)
            $polyline = $this->getPolylines($route);
            $nearestPoints = $this->getNearestPoints($points, 10, 2000);

            //echo count($polyline);
            //echo '<pre>';
            //print_r($polyline);
            //die;

            $data = [
                'id' => $route->id,
                'name' => $route->name,
                'description' => $route->description,
                'distance' => $route->distance,
                'elevation_gain' => $route->elevation_gain,
                'path' => '/user/routes'
            ];

            Mapper::polyline($polyline, ['data' => $data]);

            foreach ($nearestPoints as $nearestPoint) {
                Mapper::informationWindow($nearestPoint['lat'], $nearestPoint['lng'], $nearestPoint, ['markers' => []]);
            }

            return view('pages.route.user-route-show', compact('route'));
        } else {
            $route = Route::with('points')->findOrFail($id);

            $points = $route->points;

            // get first point and show the map
            Mapper::map($points[0]->lat, $points[0]->lng, ['center' => true]);

            // Get polyline and nearest points (<=2000m)
            $polyline = $this->getPolylines($route);
            $nearestPoints = $this->getNearestPoints($points, 10, 2000);

            $data = [
                'id' => $route->id,
                'name' => $route->name,
                'description' => $route->description,
                'distance' => $route->distance,
                'elevation_gain' => $route->elevation_gain,
                'path' => '/routes'
            ];

            Mapper::polyline($polyline, ['data' => $data]);

            foreach ($nearestPoints as $nearestPoint) {
                Mapper::informationWindow($nearestPoint['lat'], $nearestPoint['lng'], $nearestPoint, ['markers' => []]);
            }

            return view('pages.route.public-route-show', compact('route'));
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $route = Route::find($id);

        return view('pages.route.user-route-edit', compact('route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('change-access') && $request->has('public')) {
            $request->validate(['public' => 'numeric']);

            $route = Route::private()->findOrFail($id);
            $route->public = $request->get('public');
            $route->save();

            return redirect('/user/routes')->with('success', 'Данные успешно изменены');
        }

        $request->validate([
            'name' => 'required|max:255'
        ]);

        $route = Route::find($id);
        $route->name = $request->get('name');
        $route->description = $request->get('description');
        $route->save();

        return redirect('/user/routes')->with('success', 'Информация успешно изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $points = RoutePoints::where('route_id', $id)->delete();
        $route = Route::find($id)->delete();

        return redirect('/user/routes')->with('success', 'Трек успешно удален');
    }

    /**
     * Display all resources on the map.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        Mapper::map(53.250981071172895, 50.265240499999976, ['zoom' => 10, 'center' => true]);

        $prefix = $request->route()->getPrefix();
        if ($prefix == '/user') {
            $routes = Route::with('points')->private()->get();

            foreach ($routes as $route) {
                $polyline = $this->getPolylines($route);
                $color = '#' . dechex(rand(256,16777215)); // Polyline color

                $data = [
                    'id' => $route->id,
                    'name' => $route->name,
                    'description' => $route->description,
                    'distance' => $route->distance,
                    'elevation_gain' => $route->elevation_gain,
                    'path' => '/user/routes'
                ];
                Mapper::polyline($polyline, ['strokeColor' => $color, 'strokeWeight' => 3, 'data' => $data]);
            }

            return view('pages.route.user-routes-all');
        } else {
            $routes = Route::with('points')->public()->get();

            foreach ($routes as $route) {
                $polyline = $this->getPolylines($route);
                $color = '#' . dechex(rand(256,16777215)); // Polyline color

                $data = [
                    'id' => $route->id,
                    'name' => $route->name,
                    'description' => $route->description,
                    'distance' => $route->distance,
                    'elevation_gain' => $route->elevation_gain,
                    'path' => '/routes'
                ];

                Mapper::polyline($polyline, ['strokeColor' => $color, 'strokeWeight' => 3, 'data' => $data]);
            }

            return view('pages.route.public-routes-all');
        }
    }
}
