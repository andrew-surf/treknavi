<?php

namespace App\Http\Controllers;

use App\GooglePoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mapper;

class GooglePointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $prefix = $request->route()->getPrefix();
        if ($prefix == '/user') {
            $points = GooglePoint::private()->paginate(30);
            return view('pages.point.user-points-index', compact('points'));
        } else {
            $points = GooglePoint::public()->paginate(30);
            return view('pages.point.public-points-index', compact('points'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Mapper::map(53.250981071172895, 50.265240499999976, ['zoom' => 10, 'center' => true, 'setmarker' => true]);

        return view('pages.point.user-point-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:120',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ]);

        $data = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'coordinates' => DB::raw("GeomFromText('POINT(" . $request->get('lat') . " " . $request->get('lng') . ")')"),
            'altitude' => $request->get('altitude'),
            'category_id' => 1,
            'user_id' => Auth::id(),
            'visible' => 0,
        ];

        $point = new GooglePoint($data);
        $point->save();

        return redirect('/user/points')->with('success', 'Точка успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        Mapper::map(53.250981071172895, 50.265240499999976, ['zoom' => 10, 'center' => true]);

        $prefix = $request->route()->getPrefix();
        if ($prefix == '/user') {
            $point = GooglePoint::private()->findOrFail($id);
            Mapper::informationWindow($point->lat, $point->lng, $point->name, ['open' => true, 'maxWidth' => 300]);

            return view('pages.point.user-point-show', compact('point'));
        } else {
            $point = GooglePoint::public()->findOrFail($id);
            Mapper::informationWindow($point->lat, $point->lng, $point->name, ['open' => true, 'maxWidth' => 300]);

            return view('pages.point.public-point-show', compact('point'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $point = GooglePoint::private()->findOrFail($id);

        Mapper::map(53.250981071172895, 50.265240499999976, ['zoom' => 10, 'center' => true, 'setmarker' => true]);
        Mapper::marker($point->lat, $point->lng);

        return view('pages.point.user-point-edit', compact('point'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('change-access') && $request->has('public')) {
            $request->validate(['public' => 'numeric']);

            $point = GooglePoint::private()->findOrFail($id);
            $point->public = $request->get('public');
            $point->save();

            return redirect('/user/points')->with('success', 'Данные успешно изменены');
        }

        $request->validate([
            'name' => 'required|max:120',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ]);

        $point = GooglePoint::private()->findOrFail($id);
        $point->name = $request->get('name');
        $point->description = $request->get('description');
        $point->coordinates = DB::raw("GeomFromText('POINT(" . $request->get('lat') . " " . $request->get('lng') . ")')");
        $point->altitude = $request->get('altitude');
        $point->save();

        return redirect('/user/points')->with('success', 'Данные успешно изменены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $point = GooglePoint::private()->findOrFail($id);
        $point->delete();

        return redirect('/user/points')->with('success', 'Запись успешно удалена');
    }

    /**
     * Display all resources on the map.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        Mapper::map(53.250981071172895, 50.265240499999976, ['zoom' => 10, 'center' => true]);

        $prefix = $request->route()->getPrefix();
        if ($prefix == '/user') {
            $points = GooglePoint::private()->get();

            foreach ($points as $point) {
                Mapper::informationWindow($point->lat, $point->lng, $point->name, ['eventClick' => 'ajaxGetInfo(' . $point->id . ', "/user/points/ajaxgetinfo");', 'open' => true, 'maxWidth' => 300]);
            }

            return view('pages.point.user-points-all', compact('points'));
        } else {
            $points = GooglePoint::public()->get();

            foreach ($points as $point) {
                Mapper::informationWindow($point->lat, $point->lng, $point->name, ['eventClick' => 'ajaxGetInfo(' . $point->id . ', "/points/ajaxgetinfo");', 'open' => true, 'maxWidth' => 300]);
            }

            return view('pages.point.public-points-all', compact('points'));
        }
    }
}
