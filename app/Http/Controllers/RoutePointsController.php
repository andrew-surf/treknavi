<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatPoints;
use App\GooglePoint;
use Mapper;
use App\Http\Traits\GisFunction;

class RoutePointsController extends Controller
{
    use GisFunction;

    /**
     * Display a listing of the resources
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $categories = CatPoints::withCount('points')->get();

        return view('pages.point.points-page', compact('categories'));
    }

    /**
     * Show category of points
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function showPointsCat($id)
    {
        $catName = CatPoints::find($id);
        $pointslist = GooglePoint::where('category_id', $id)->get();

        return view('pages.point.points-cat-page')->with('pointslist', $pointslist)->with('catname', $catName);
    }

    /**
     * Display the specified resource on the map
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function showPointOnMap($cat, $id, Request $request)
    {
        $point = GooglePoint::find($id);
        $category = CatPoints::find($cat);

        //dd($point->toArray());

        Mapper::map($point->lat, $point->lng, ['zoom' => 11, 'center' => true]);
        Mapper::informationWindow($point->lat, $point->lng, $point->toArray());

        if ($request->input('show') && $request->input('show') == 'routes') {
            $routes = $this->getNearestRoutes($point, 2000); // 2000 meters

            foreach ($routes as $route) {
                $polyline = $this->getPolylines($route);

                // Polyline color generation
                $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

                $data = [
                    'id' => $route->id,
                    'name' => $route->name,
                    'description' => $route->description,
                    'distance' => $route->distance
                ];

                Mapper::polyline($polyline, ['strokeColor' => $color, 'strokeWeight' => 3, 'data' => $data]);
            }
        }

        return view('pages.point.point-map', ['point' => $point, 'category' => $category]);
    }
}
