<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoutePoints extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'route_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['trek_id','coordinates','altitude'];

    /**
     * Define the name of column in a table.
     *
     * @var string
     */
    protected $column = 'coordinates';

    /**
     * Select geometrical attributes as text from database.
     *
     * @var bool
     */
    protected $geometryAsText = true;

    /**
     * Get a new query builder for the model's table.
     * Manipulate in case we need to convert geometrical fields to text.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function newQuery()
    {

        if ($this->geometryAsText === true) {
            return parent::newQuery()->select('*',
                DB::raw('X(' . $this->column . ') AS lat, 
                    Y(' . $this->column . ') AS lng'));
        }

    }

    /**
     * Get the route that owns the points.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route()
    {
        return $this->belongsTo('App\Route');
    }
}
