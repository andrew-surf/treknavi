<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatPoints extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cat_points';

    /**
     * Get the points that owns the category.
     */
    public function points()
    {
        return $this->belongsTo('App\GooglePoint','id','category_id');
    }
}
