<?php

use Illuminate\Database\Seeder;

class RoutePointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\RoutePoints::class, 5)->create();
    }
}
