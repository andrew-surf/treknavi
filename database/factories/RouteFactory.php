<?php

use Faker\Generator as Faker;

$factory->define(App\Route::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomDigit,
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'distance' => $faker->randomFloat,
    ];
});
