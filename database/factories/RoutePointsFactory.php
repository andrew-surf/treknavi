<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(App\RoutePoints::class, function (Faker $faker) {
    return [
        'trek_id' => $faker->randomDigit,
        'coordinates' => DB::raw("GeomFromText('POINT(" . $faker->latitude . " " . $faker->longitude . ")')"),
        'altitude' => $faker->randomFloat,
    ];
});
