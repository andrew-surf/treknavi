$(function () {

    // MetisMenu
    $('body').addClass('loaded');

    $('#sidebar-menu, #customize-menu').metisMenu({
        activeClass: 'open'
    });

    // Show and hide menu
    $('#trek_info .trek_menu').click(function () {
        if ($('#trek_info .card-block').is(':visible')) {
            $('#trek_info .card-block').slideUp('fast');
            $('#trek_info .trek_menu i').removeClass().addClass('fa fa-plus-square');
        } else {
            $('#trek_info .card-block').slideDown('fast');
            $('#trek_info .trek_menu i').removeClass().addClass('fa fa-minus-square');
        }
    });

    // Delete object
    $('.dropleft #actions').click(function () {
        var route = $(this).parent().find('.destroy').data('route');
        $('#modal-actions #actions-form').attr('action', route);
    });

    // Validation form
    // Create point form
    $('#create_point').submit(function () {
        if (validate_form(['name', 'altitude', 'lat', 'lng']) === false) {
            return false;
        }
    });

    // Create route form
    $('#create_route').submit(function () {
        if (validate_form(['name']) === false) {
            return false;
        }
    });
});

function validate_form(arr) {
    var error = false;
    var values = arr;

    $.each(values, function (index, value) {
        var elem = $('.card-block #' + value.toString());
        elem.parent().removeClass('has-error');

        if (elem.val() == '') {
            elem.parent().addClass('has-error');
            error = true;
        }
    });

    if (error === true) return false;
}
