<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ===============================================
// STATIC PAGES ==================================
// ===============================================

// Display all resources on the map
Route::get('/', 'RouteController@all');

// About page
Route::view('/about', 'pages/about');

// Features page
Route::view('/features', 'pages/features');

// ===============================================
// PUBLIC ROUTES PAGES ====================================
// ===============================================

// Display resources on the map
Route::get('/routes/all', 'RouteController@all');

// Display the specified resource on the map
Route::get('/routes/{id}', 'RouteController@show');

// Display a listing of the resources
Route::get('/routes', 'RouteController@index');

// ===============================================
// POINTS PAGES ==================================
// ===============================================

// Display a listing of the resources
Route::get('points', 'GooglePointController@index');

// Display resources on the map
Route::get('points/all', 'GooglePointController@all');
Route::post('points/ajaxgetinfo', 'AjaxController@getPointInfo');

// Show category of points
Route::get('points/{id}', 'GooglePointController@show');

// Display the specified resource on the map
Route::get('points/{cat}/{id}', 'RoutePointsController@showPointOnMap');

// ===============================================
// USER AREA =====================================
// ===============================================

Route::prefix('user')->group(function () {
    Auth::routes();

    Route::get('routes/all', 'RouteController@all')->middleware('auth');
    Route::resource('routes', 'RouteController')->middleware('auth');

    Route::get('points/all', 'GooglePointController@all')->middleware('auth');
    Route::post('points/ajaxgetinfo', 'AjaxController@getPointInfo')->middleware('auth');
    Route::resource('points', 'GooglePointController')->middleware('auth');
});


