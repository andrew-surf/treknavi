<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function a_guest_should_be_able_to_see_the_login_page()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function a_guest_should_be_able_to_see_the_register_page()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function a_loggedin_user_should_not_be_able_to_see_the_login_page()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get('/login');

        $response->assertRedirect('/');
    }

    /**
     * @test
     */
    public function a_loggedin_user_should_not_be_able_to_see_the_register_page()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get('/register');

        $response->assertRedirect('/');
    }

    /**
     * @test
     */
    public function a_guest_should_be_able_to_login_with_valid_credentials()
    {
        $user = factory(User::class)->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);
        $response->assertStatus(302);
        $this->assertAuthenticatedAs($user);
    }

    /**
     * @test
     */
    public function a_loggedin_user_can_see_the_dashboard()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get('/');

        $response->assertOk();
    }
}
