<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Login' => 'Вход',
    'Register' => 'Регистрация',
    'Logout' => 'Выход',
    'Forgot Your Password?' => 'Забыли пароль?',
    'Remember Me' => 'запомнить меня',
    'Name' => 'Имя',
    'E-Mail Address' => 'E-mail',
    'Password' => 'Пароль',
    'Confirm Password' => 'Повторите пароль',
    'Reset Password' => 'Сбросить пароль',
    'Send Password Reset Link' => 'Отправить ссылку',
    'failed' => 'Проверьте правильность ввода email и пароля',
    'throttle' => 'Слишком много попыток. Попробуйте еще раз через :seconds секунд.',

];
