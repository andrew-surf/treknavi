var polylineCoordinates_{!! $id !!} = [
	@foreach ($options['coordinates'] as $coordinate)
		new google.maps.LatLng({!! $coordinate['latitude'] !!}, {!! $coordinate['longitude'] !!}),
	@endforeach
];

var polyline_{!! $id !!} = new google.maps.Polyline({
	path: polylineCoordinates_{!! $id !!},
	geodesic: {!! $options['strokeColor'] ? 'true' : 'false' !!},
	strokeColor: '{!! $options['strokeColor'] !!}',
	strokeOpacity: {!! $options['strokeOpacity'] !!},
	strokeWeight: {!! $options['strokeWeight'] !!},
	editable: {!! $options['editable'] ? 'true' : 'false' !!}
});

var tmpl = '<div class="infowindow">' + 
	       '<a class="name" href="{!! $options['data']['path'] !!}/{!! $options['data']['id'] !!}">{!! $options['data']['name'] !!}</a>';

var infowindow_{!! $id !!} = new google.maps.InfoWindow({
	content: tmpl,
	maxWidth: 300
});

google.maps.event.addListener(polyline_{!! $id !!}, 'click', function(event) {
	infowindow_{!! $id !!}.setPosition(event.latLng);
	infowindow_{!! $id !!}.open({!! $options['map'] !!});
    filldata();
});

polyline_{!! $id !!}.setMap({!! $options['map'] !!});

shapes.push({
	'polyline_{!! $id !!}': polyline_{!! $id !!}
});

function filldata() {
    $('.card-block input[name=name]').val('{!! $options['data']['name'] !!}');
    $('.card-block textarea[name=description]').val('{!! $options['data']['description'] !!}');
    $('.card-block input[name=distance]').val('{!! $options['data']['distance'] !!}');
    $('.card-block input[name=elevation_gain]').val('{!! $options['data']['elevation_gain'] !!}');
}
