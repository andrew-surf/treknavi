<div id="map-canvas-{!! $id !!}" style="width: 100%; height: 100%; margin: 0; padding: 0; position: relative; overflow: hidden;"></div>

<script type="text/javascript">

	var maps = [];

	function initialize_{!! $id !!}() {
		var bounds = new google.maps.LatLngBounds();
		var infowindow = new google.maps.InfoWindow();
		var position = new google.maps.LatLng({!! $options['latitude'] !!}, {!! $options['longitude'] !!});

		var mapOptions_{!! $id !!} = {
			@if ($options['center'])
				center: position,
			@endif
            zoom: {!! $options['zoom'] !!},
			mapTypeId: google.maps.MapTypeId.{!! $options['type'] !!},
			disableDefaultUI: @if (!$options['ui']) true @else false @endif,
			scrollwheel: @if ($options['scrollWheelZoom']) true @else false @endif,
            zoomControl: @if ($options['zoomControl']) true @else false @endif,
            mapTypeControl: @if ($options['mapTypeControl']) true @else false @endif,
            scaleControl: @if ($options['scaleControl']) true @else false @endif,
            streetViewControl: @if ($options['streetViewControl']) true @else false @endif,
            rotateControl: @if ($options['rotateControl']) true @else false @endif,
            fullscreenControl: @if ($options['fullscreenControl']) true @else false @endif,
            zoomControlOptions: { position: google.maps.ControlPosition.LEFT_CENTER }
		};

		var map_{!! $id !!} = new google.maps.Map(document.getElementById('map-canvas-{!! $id !!}'), mapOptions_{!! $id !!});
		map_{!! $id !!}.setTilt({!! $options['tilt'] !!});

		var markers = [];
		var infowindows = [];
		var shapes = [];

		@foreach ($options['markers'] as $key => $marker)
			{!! $marker->render($key, $view) !!}
		@endforeach

		@foreach ($options['shapes'] as $key => $shape)
			{!! $shape->render($key, $view) !!}
		@endforeach

		@if ($options['overlay'] == 'BIKE')
			var bikeLayer = new google.maps.BicyclingLayer();
			bikeLayer.setMap(map_{!! $id !!});
		@endif

		@if ($options['overlay'] == 'TRANSIT')
			var transitLayer = new google.maps.TransitLayer();
			transitLayer.setMap(map_{!! $id !!});
		@endif

		@if ($options['overlay'] == 'TRAFFIC')
			var trafficLayer = new google.maps.TrafficLayer();
			trafficLayer.setMap(map_{!! $id !!});
		@endif

        @if (isset($options['setmarker']))
            {!! $options['map'] !!}.addListener('click', function(event) {
                deleteMarkers(); 
                addMarker(event.latLng);
            });

            $('#delete_marker').click(function () {
                $('.card-block #lat').val('');
                $('.card-block #lng').val('');
                $('.card-block #altitude').val('');
                $('.card-block input[name=name]').val('');
                $('.card-block textarea[name=description]').val('');
                $('.card-block .form-group').eq(0).removeClass('has-error');
                $('.card-block .form-group').eq(2).removeClass('has-error');
                deleteMarkers();
            });

            function addMarker(location) {
                var marker = new google.maps.Marker({
                    position: location,
                    draggable: true,
                    map: {!! $options['map'] !!}
                });

                var elevator = new google.maps.ElevationService;

                elevator.getElevationForLocations({
                    'locations': [location]
                }, function(results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            $('.card-block #altitude').val(results[0].elevation.toFixed(2));
                        } else {
                            console.log('no results')
                        }
                    } else {
                        console.log('Elevation service failed due to: ' + status);
                    }
                });



                google.maps.event.addListener(marker, 'dragend', function() {
                    var location = this.position;
                    $('.card-block #lat').val(location.lat());
                    $('.card-block #lng').val(location.lng());
                });

                $('.card-block #lat').val(location.lat());
                $('.card-block #lng').val(location.lng());

                markers.push(marker);
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            function clearMarkers() {
                setMapOnAll(null);
            }
        @endif

        var idleListener = google.maps.event.addListenerOnce(map_{!! $id !!}, "idle", function () {
			map_{!! $id !!}.setZoom({!! $options['zoom'] !!});

			@if (!$options['center'])
				map_{!! $id !!}.fitBounds(bounds);
			@endif

			@if ($options['locate'])
				if (typeof navigator !== 'undefined' && navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function (position) {
						map_{!! $id !!}.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
					});
				}
			@endif
		});

        var map = map_{!! $id !!};

		@if (isset($options['eventBeforeLoad']))
			{!! $options['eventBeforeLoad'] !!}
		@endif

		@if (isset($options['eventAfterLoad']))
			google.maps.event.addListenerOnce(map_{!! $id !!}, "tilesloaded", function() {
				{!! $options['eventAfterLoad'] !!}
			});
		@endif

		@if ($options['cluster'])
			var markerClusterOptions = {
				imagePath: '{!! $options['clusters']['icon'] !!}',
				gridSize: {!! $options['clusters']['grid'] !!},
				maxZoom: @if ($options['clusters']['zoom'] === null) null @else {!! $options['clusters']['zoom'] !!} @endif,
				averageCenter: @if ($options['clusters']['center'] === true) true @else false @endif,
				minimumClusterSize: {!! $options['clusters']['size'] !!}
			};
			var markerCluster = new MarkerClusterer(map_{!! $id !!}, markers, markerClusterOptions);
		@endif

		maps.push({
			key: {!! $id !!},
			markers: markers,
			infowindows: infowindows,
			map: map_{!! $id !!},
			shapes: shapes
		});
	}

    @if (!$options['async'])

	    google.maps.event.addDomListener(window, 'load', initialize_{!! $id !!});

    @endif

</script>