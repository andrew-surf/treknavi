<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <a href="/" alt="Главная" title="Главная">
                    <div class="logo">
                        <img src="{{ asset('assets/logo.png') }}"/>
                    </div>
                Навигатор
                </a>
            </div>
        </div>
        <nav class="menu">
            <ul class="sidebar-menu metismenu" id="sidebar-menu">
                <li class="active">
                    <a href="/"><i class="fa fa-home"></i> Главная</a>
                </li>
                <li>
                    <a href="/about"><i class="fa fa-file-text-o"></i> О проекте</a>
                </li>
                <li>
                    <a href="/features"><i class="fa fa-file-text-o"></i> Возможности</a>
                </li>
                @auth
                    <li>
                        <a href="">
                            <i class="fa fa-bicycle"></i> Мои данные
                            <i class="fa arrow"></i>
                        </a>
                        <ul class="sidebar-nav">
                            <li>
                                <a href="/user/routes/create"> Добавить трек</a>
                            </li>
                            <li>
                                <a href="/user/points/create"> Добавить точку</a>
                            </li>
                            <li>
                                <a href="/user/routes"> Мои треки</a>
                            </li>
                            <li>
                                <a href="/user/points"> Мои точки</a>
                            </li>
                        </ul>
                    </li>
                @endauth
                <li>
                    <a href="">
                        <i class="fa fa-table"></i> Данные
                        <i class="fa arrow"></i>
                    </a>
                    <ul class="sidebar-nav">
                        <li>
                            <a href="/toponimics"> Топонимика</a>
                        </li>
                        <li>
                            <a href="/routes"> Треки</a>
                        </li>
                        <li>
                            <a href="/points"> Точки</a>
                        </li>
                        <li>
                            <a href="#"> Карты </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="">
                        <i class="fa fa-cogs"></i> Инструменты
                        <i class="fa arrow"></i>
                    </a>
                    <ul class="sidebar-nav">
                        <li>
                            <a href=""> Просмотр PLT и PWT</a>
                        </li>
                        <li>
                            <a href="#"> Конвертер </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="">
                        <i class="fa fa-files-o"></i> Файлы
                        <i class="fa arrow"></i>
                    </a>
                    <ul class="sidebar-nav">
                        <li>
                            <a href=""> PLT и WPT (Ozi)</a>
                        </li>
                        <li>
                            <a href=""> Самарская Лука</a>
                        </li>
                        <li>
                            <a href=""> Мои маршруты</a>
                        </li>
                        <li>
                            <a href=""> Дорожки и парковки</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-comments"></i> Форум </a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-external-link"></i> Сайт </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>