@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page">
        <div class="title-block">
            <h1 class="title"> Точки </h1>
            <p class="title-description"> Выберите интересующую категорию или воспользуйтесь поиском </p>
        </div>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> Категория "{{ $catname->name }}"</h3>
                            </div>
                            <section class="example">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="align-middle">Название</th>
                                            <th class="align-middle">Координаты</th>
                                            <th class="align-middle">Ближайшие треки</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($pointslist as $point)
                                            <tr>
                                                <td>
                                                    <a href="/points/{{ $point->vcCategory_id }}/{{ $point->GoogleMapPointID }}">{{ $point->vcName }}</a>
                                                    @if ($point->vcDescription)
                                                        <br>
                                                        <small>{!! $point->vcDescription !!}</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $point->lat }}
                                                    <br>
                                                    {{ $point->lng }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@stop