@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page" style="position:relative;padding: 70px 0 0 0;">
        <div id="map_canvas">
            {!! Mapper::render() !!}
        </div>
        <div id="trek_info">
            <div class="card card-info">
                <div class="card-header">
                    <div class="header-block">
                        <div class="title"> Информация <span class="pull-right trek_menu"><i
                                        class="fa fa-minus-square" aria-hidden="true"></i></span></div>
                    </div>
                </div>
                <div class="card-block">
                    <form action="{{ route('points.update', $point->id) }}" method="post" id="create_point">
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label class="control-label bolder">Название *:</label>
                            <input type="text" name="name" class="form-control boxed" value="{{ $point->name }}">
                            <span class="has-error" style="display: none;">Заполните поле</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Описание:</label>
                            <textarea rows="3" name="description" class="form-control boxed">{{ $point->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Высота:</label>
                            <input type="text" id="altitude" name="altitude" readonly="readonly" class="form-control boxed" value="{{ $point->altitude }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Координаты:</label>
                            <input type="text" id="lat" name="lat" readonly="readonly" class="form-control boxed" value="{{ $point->lat }}"> <br>
                            <input type="text" id="lng" name="lng" readonly="readonly" class="form-control boxed" value="{{ $point->lng }}">
                            <span class="has-error" style="display: none;">Укажите точку</span>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-danger" id="delete_marker">Очистить</button>
                        <a href="/user/points" style="float:right;">Назад</a>
                    </form>
                </div>
            </div>
        </div>
    </article>
@stop