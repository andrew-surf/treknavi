@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page" style="position:relative;padding: 70px 0 0 0;">
        <div id="map_canvas">
            {!! Mapper::render() !!}
        </div>
        <div id="trek_info">
            <div class="card card-info">
                <div class="card-header">
                    <div class="header-block">
                        <div class="title"> Информация <span class="pull-right trek_menu"><i
                                        class="fa fa-minus-square" aria-hidden="true"></i></span></div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form-group">
                        <label class="control-label bolder">Название:</label>
                        <input disabled="disabled" type="text" name="name" class="form-control boxed" value="">
                        <span class="has-error" style="display: none;">Заполните поле</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Описание:</label>
                        <textarea disabled="disabled" rows="3" name="description" class="form-control boxed"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Высота:</label>
                        <input type="text" id="altitude" name="altitude" readonly="readonly" class="form-control boxed" value="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Координаты:</label>
                        <input type="text" id="lat" name="lat" disabled="disabled" class="form-control boxed" value=""> <br>
                        <input type="text" id="lng" name="lng" disabled="disabled" class="form-control boxed" value="">
                        <span class="has-error" style="display: none;">Укажите точку</span>
                    </div>
                    <hr>
                    <a href="/user/points">Назад</a>
                </div>
            </div>
        </div>
    </article>
@stop