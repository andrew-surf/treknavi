@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page" style="position:relative;padding: 70px 0 0 0;">
        <div id="map_canvas">
            {!! Mapper::render() !!}
        </div>
        <div id="trek_info">
            <div class="card card-info">
                <div class="card-header">
                    <div class="header-block">
                        <div class="title"> {{ $point->vcName }} <span class="pull-right trek_menu"><i
                                        class="fa fa-minus-square" aria-hidden="true"></i></span></div>
                    </div>
                </div>
                <div class="card-block">
                    <p>Категория: <a href="/points/{{ $category->vcCategory_id }}">{{ $category->name }}</a></p>
                    <p>Координаты: {{ $point->lat }}, {{ $point->lng }}</p>
                    <p>Описание:
                        @if(empty($point->vcDescription))
                            отсутствует
                        @else
                            {{ $point->vcDescription }}
                        @endif
                    </p>
                    <hr>
                    <p><a href="{{url()->current()}}?show=routes">Ближайшие к точке треки</a></p>
                    <p><a href="{{ url()->previous() }}">Вернуться назад</a></p>
                </div>
            </div>
        </div>
    </article>
@stop