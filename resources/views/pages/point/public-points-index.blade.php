@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page">
        <div class="title-block">
            <h1 class="title"> Точки </h1>
            <p class="title-description"> Здесь собраны все точки, которыми поделились пользователи, а также добавленные администратором </p>
        </div>
        <p>
            <a href="/points/all" class="btn btn-success">Показать на карте</a>
        </p>

        @if(session()->get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ session()->get('success') }}
            </div>
        @endif

        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> Список точек </h3>
                            </div>
                            <section class="example">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="align-middle text-center">#</th>
                                            <th class="align-middle text-left">Название</th>
                                            <th class="align-middle text-left">Описание</th>
                                            <th class="align-middle text-center">Координаты</th>
                                            <th class="align-middle text-center">Высота(м)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($points as $k => $point)
                                            <tr>
                                                <td class="text-center">{{ $k+1 }}</td>
                                                <td><a href="/points/{{ $point->id }}">{{ $point->name }}</a></td>
                                                <td>{{ $point->description }}</td>
                                                <td class="text-center">
                                                    {{ $point->lat }}
                                                    <br>
                                                    {{ $point->lng }}
                                                </td>
                                                <td class="text-center">
                                                    @if ($point->altitude)
                                                        {{ $point->altitude }}
                                                    @else
                                                        -
                                                    @endif</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $points->links() }}
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>

    <!-- Modal -->
    <div class="modal fade" id="modal-actions" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Удалить элемент</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    Уверены, что хотите удалить элемент?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                    <form action="" method="post" id="actions-form">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Да</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
