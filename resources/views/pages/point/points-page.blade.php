@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page">
        <div class="title-block">
            <h1 class="title"> Точки </h1>
            <p class="title-description"> Выберите интересующую категорию или воспользуйтесь поиском </p>
        </div>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> Категории точек </h3>
                            </div>
                            <section class="example">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="align-middle">Категория</th>
                                            <th class="align-middle">Количество</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($categories as $category)
                                            <tr>
                                                <td><a href="/points/{{ $category->vcCategory_id }}">{{ $category->name }}</a></td>
                                                <td>{{ $category->points_count }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@stop