@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page">
        <div class="title-block">
            <h1 class="title"> Мои точки </h1>
        </div>
        <p>
            <a href="/user/points/create" class="btn btn-primary">Добавить точку</a>
            <a href="/user/points/all" class="btn btn-success">Показать на карте</a>
        </p>
        @if(session()->get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ session()->get('success') }}
            </div>
        @endif
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> Список точек </h3>
                            </div>
                            <section class="">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="align-middle text-center">#</th>
                                            <th class="align-middle text-left">Название</th>
                                            <th class="align-middle text-left">Описание</th>
                                            <th class="align-middle text-center">Координаты</th>
                                            <th class="align-middle text-center">Высота(м)</th>
                                            <th class="align-middle text-center">Действия</th>
                                            <th class="align-middle text-center">Статус</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($points as $k => $point)
                                            <tr>
                                                <td class="text-center">{{ $k+1 }}</td>
                                                <td><a href="/user/points/{{ $point->id }}">{{ $point->name }}</a></td>
                                                <td>{{ $point->description }}</td>
                                                <td class="text-center">
                                                    {{ $point->lat }}
                                                    <br>
                                                    {{ $point->lng }}
                                                </td>
                                                <td class="text-center">
                                                    @if ($point->altitude)
                                                        {{ $point->altitude }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center" style="width: 100px;">
                                                    <div class="btn-group dropleft">
                                                        <i id="actions" class="fa fa-cog" aria-hidden="true"
                                                           data-toggle="dropdown" aria-haspopup="true"
                                                           aria-expanded="false"></i>
                                                        <div class="dropdown-menu">
                                                            <a href="/user/points/{{ $point->id }}/edit" alt="изменить"
                                                               title="изменить" class="dropdown-item">Изменить</a>
                                                            <form action="{{ route('points.update', $point->id)}}" method="post" id="actions-form">
                                                                @csrf
                                                                @method('PATCH')
                                                                @if ($point->public == 0)
                                                                    <input name="public" type="hidden" value="1">
                                                                    <input name="change-access" type="hidden" value="1">
                                                                    <button style="cursor: pointer;" class="dropdown-item" type="submit">Общий доступ</button>
                                                                @elseif ($point->public == 1)
                                                                    <input name="public" type="hidden" value="0">
                                                                    <input name="change-access" type="hidden" value="0">
                                                                    <button style="cursor: pointer;" class="dropdown-item" type="submit">Приватный</button>
                                                                @endif
                                                            </form>
                                                            <button type="button" style="cursor: pointer;"
                                                                    class="dropdown-item destroy"
                                                                    data-route="{{ route('points.destroy', $point->id)}}"
                                                                    data-toggle="modal" data-target="#modal-actions">
                                                                Удалить
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    @if ($point->public == 1)
                                                        <i class="fa fa-share-alt" aria-hidden="true"
                                                           title="Общий доступ"></i>
                                                    @elseif ($point->public == 0)
                                                        <i class="fa fa-lock" aria-hidden="true" title="Приватный"></i>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $points->links() }}
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>

    <!-- Modal -->
    <div class="modal fade" id="modal-actions" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Удалить элемент</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    Уверены, что хотите удалить элемент?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                    <form action="" method="post" id="actions-form">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Да</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
