@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page" style="position:relative;padding: 70px 0 0 0;">
        <div id="map_canvas">
            {!! Mapper::render() !!}
        </div>
        <div id="trek_info">
            <div class="card card-info">
                <div class="card-header">
                    <div class="header-block">
                        <div class="title">
                            @if(session()->get('success'))
                                {{ session()->get('success') }}
                            @else
                                Информация
                            @endif
                            <span class="pull-right trek_menu"><i
                                    class="fa fa-minus-square" aria-hidden="true"></i></span></div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="form-group">
                        <label class="control-label bolder">Название:</label>
                        <input disabled="disabled" type="text" name="name" class="form-control boxed" value="{{ $route->name }}">
                        <span class="has-error" style="display: none;">Заполните поле</span>
                    </div>
                    @if ($route->description)
                        <div class="form-group">
                            <label class="control-label">Описание:</label>
                            <textarea disabled="disabled" rows="3" name="description" class="form-control boxed">{{ $route->description }}</textarea>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="control-label">Дистанция(км):</label>
                        <input type="text" id="distance" name="distance" readonly="readonly" class="form-control boxed" value="{{ $route->distance }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Набор высоты(м):</label>
                        <input type="text" id="elevation_gain" name="elevation_gain" readonly="readonly" class="form-control boxed" value="{{ $route->elevation_gain }}">
                    </div>
                    @if ($route->duration)
                        <div class="form-group">
                            <label class="control-label">Время:</label>
                            <input type="text" id="duration" name="duration" readonly="readonly" class="form-control boxed" value="{{ $route->duration }}">
                        </div>
                    @endif
                    <hr>
                    <a href="/routes">К списку треков</a>
                </div>
            </div>
        </div>
    </article>
@stop
