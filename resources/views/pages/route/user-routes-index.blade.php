@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page">
        <div class="title-block">
            <h1 class="title"> Мои треки </h1>
            <p class="title-description"> Добавьте свой трек в формате gpx </p>
        </div>
        <p>
            <a href="/user/routes/create" class="btn btn-primary">Добавить трек</a>
            <a href="/user/routes/all" class="btn btn-success">Показать на карте</a>
        </p>
        @if(session()->get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ session()->get('success') }}
            </div>
        @endif
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> Список треков </h3>
                            </div>
                            <section class="example">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="align-middle text-left">Название</th>
                                            <th class="align-middle text-center">Длина(км)</th>
                                            <th class="align-middle text-center">Мин.высота(м)</th>
                                            <th class="align-middle text-center">Макс.высота(м)</th>
                                            <th class="align-middle text-center">Набор высоты(м)</th>
                                            <th class="align-middle text-center">Время(ч)</th>
                                            <th class="align-middle text-center">Действия</th>
                                            <th class="align-middle text-center">Статус</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($routes as $route)
                                            <tr>
                                                <td><a href="/user/routes/{{ $route->id }}">{{ $route->name }}</a></td>
                                                <td class="text-center">
                                                    @if ($route->distance)
                                                        {{ $route->distance }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->min_alt)
                                                        {{ $route->min_alt }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->max_alt)
                                                        {{ $route->max_alt }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->elevation_gain)
                                                        {{ $route->elevation_gain }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->duration)
                                                        {{ $route->duration }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center" style="width: 100px;">
                                                    <div class="btn-group dropleft">
                                                        <i id="actions" class="fa fa-cog" aria-hidden="true"
                                                           data-toggle="dropdown" aria-haspopup="true"
                                                           aria-expanded="false"></i>
                                                        <div class="dropdown-menu">
                                                            <a href="/user/routes/{{ $route->id }}/edit" alt="изменить"
                                                               title="изменить" class="dropdown-item">Изменить</a>
                                                            <form action="{{ route('routes.update', $route->id)}}" method="post" id="actions-form">
                                                                @csrf
                                                                @method('PATCH')
                                                                @if ($route->public == 0)
                                                                    <input name="public" type="hidden" value="1">
                                                                    <input name="change-access" type="hidden" value="1">
                                                                    <button style="cursor: pointer;" class="dropdown-item" type="submit">Общий доступ</button>
                                                                @elseif ($route->public == 1)
                                                                    <input name="public" type="hidden" value="0">
                                                                    <input name="change-access" type="hidden" value="0">
                                                                    <button style="cursor: pointer;" class="dropdown-item" type="submit">Приватный</button>
                                                                @endif
                                                            </form>
                                                            <button type="button" style="cursor: pointer;"
                                                                    class="dropdown-item destroy"
                                                                    data-route="{{ route('routes.destroy', $route->id)}}"
                                                                    data-toggle="modal" data-target="#modal-actions">
                                                                Удалить
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->public == 1)
                                                        <i class="fa fa-share-alt" aria-hidden="true"
                                                           title="Общий доступ"></i>
                                                    @elseif ($route->public == 0)
                                                        <i class="fa fa-lock" aria-hidden="true" title="Приватный"></i>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $routes->links() }}
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>

    <!-- Modal -->
    <div class="modal fade" id="modal-actions" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Удалить элемент</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    Уверены, что хотите удалить элемент?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Нет</button>
                    <form action="" method="post" id="actions-form">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Да</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
