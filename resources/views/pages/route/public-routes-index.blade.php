@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page">
        <div class="title-block">
            <h1 class="title"> Треки </h1>
            <p class="title-description"> Выберите интересующий трек или воспользуйтесь поиском </p>
        </div>
        <p>
            <a href="/routes/all" class="btn btn-success">Показать на карте</a>
        </p>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="card-title-block">
                                <h3 class="title"> Список треков </h3>
                            </div>
                            <section class="example">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="align-middle">Название</th>
                                            <th class="align-middle">Длина(км)</th>
                                            <th class="align-middle">Мин.высота(м)</th>
                                            <th class="align-middle">Макс.высота(м)</th>
                                            <th class="align-middle">Набор высоты(м)</th>
                                            <th class="align-middle">Время(ч)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($routes as $route)
                                            <tr>
                                                <td><a href="/routes/{{ $route->id }}">{{ $route->name }}</a></td>
                                                <td class="text-center">
                                                    @if ($route->distance)
                                                        {{ $route->distance }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->min_alt)
                                                        {{ $route->min_alt }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->max_alt)
                                                        {{ $route->max_alt }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->elevation_gain)
                                                        {{ $route->elevation_gain }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($route->duration)
                                                        {{ $route->duration }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $routes->links() }}
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@stop
