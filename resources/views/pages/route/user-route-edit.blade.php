@extends('layouts.default')

@section('content')
    <article class="content typography-page">
        <div class="title-block">
            <h3 class="title"> Редактирование</h3>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ $message }}
            </div>

        @endif

        <form action="{{ route('routes.update', $route->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="card card-block">
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label text-xs-right"> Название: </label>
                    <div class="col-sm-10">
                        <input type="text" name="name" value="{{ $route->name }}" class="form-control boxed" placeholder="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label text-xs-right"> Описание: </label>
                    <div class="col-sm-10">
                        <textarea rows="5" name="description" class="form-control boxed">{{ $route->description }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button type="submit" class="btn btn-primary"> Отправить </button>
                    </div>
                </div>
            </div>
        </form>
    </article>
@stop