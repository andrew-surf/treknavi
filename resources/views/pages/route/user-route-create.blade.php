@extends('layouts.default')

@section('content')
    <article class="content responsive-tables-page" style="position:relative;padding: 70px 0 0 0;">
        <div id="map_canvas">
            {!! Mapper::render() !!}
        </div>
        <div id="trek_info">
            <div class="card card-info">
                <div class="card-header">
                    <div class="header-block">
                        <div class="title"> Информация <span class="pull-right trek_menu"><i
                                        class="fa fa-minus-square" aria-hidden="true"></i></span></div>
                    </div>
                </div>
                <div class="card-block">
                    <form action="{{ route('routes.store') }}" method="post" id="create_route" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Файл(.gpx):</label>
                            <input id="file" type="file" name="file" class="form-control-file" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label bolder">Название *:</label>
                            <input id="name" type="text" name="name" class="form-control boxed">
                            <span class="has-error" style="display: none;">Заполните поле</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Описание:</label>
                            <textarea id="description" rows="3" name="description" class="form-control boxed"></textarea>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-danger" id="delete_marker">Очистить</button>
                        <a href="/user/routes" style="float:right;">Назад</a>
                    </form>
                </div>
            </div>
        </div>
    </article>
@stop
